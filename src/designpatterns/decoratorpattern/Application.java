package designpatterns.decoratorpattern;

public class Application {
    public void needBird(Bird bird) {
        int flyDistance = bird.fly();
        System.out.println("This bird can fly " + flyDistance + " meters.");
    }
    public static void main(String[] args) {
        Application client = new Application();
        Bird sparrow = new Sparrow();
        Bird sparrowDecorator = new SparrowDecorator(sparrow);
        Bird sparrowDecoratorDecorator = new SparrowDecorator(sparrowDecorator);
        client.needBird(sparrowDecorator);
        client.needBird(sparrowDecoratorDecorator);
    }
}
