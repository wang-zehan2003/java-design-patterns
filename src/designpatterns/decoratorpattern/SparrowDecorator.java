package designpatterns.decoratorpattern;

public class SparrowDecorator extends Decorator{
    public final int DISTANCE = 50;
    public SparrowDecorator(Bird bird) {
        super(bird);
    }

    @Override
    public int fly() {
        int distance = bird.fly() + eleFly();
        return distance;
    }
    public int eleFly() {
        return DISTANCE;
    }
}
