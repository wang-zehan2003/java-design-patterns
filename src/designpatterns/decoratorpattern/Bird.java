package designpatterns.decoratorpattern;

public abstract class Bird {
    public abstract int fly();
}
