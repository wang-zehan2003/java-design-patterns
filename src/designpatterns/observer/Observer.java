package designpatterns.observer;

public interface Observer {
    public void hearTelephone(String heardMessage);
}
