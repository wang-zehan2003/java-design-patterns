package designpatterns.observer;

public class ObserverApp {
    public static void main(String[] args) {
        SeekJobCenter jobCenter = new SeekJobCenter();
        UniversityStudent zhangLin = new UniversityStudent(jobCenter, "A.txt");
        HaiGui wangHao = new HaiGui(jobCenter, "B.txt");

        jobCenter.giveNewMessage("招聘信息：阿里公司需要10个java程序员");
        jobCenter.notifyObservers();
        jobCenter.giveNewMessage("招聘信息：腾讯公司需要9个动画设计师");
        jobCenter.notifyObservers();
        jobCenter.giveNewMessage("招聘信息：百度公司需要8个软件测试工程师");
        jobCenter.notifyObservers();
        jobCenter.giveNewMessage("招聘信息：新浪公司需要7个项目经理");
        jobCenter.notifyObservers();
    }
}
