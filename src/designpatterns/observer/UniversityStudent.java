package designpatterns.observer;

import java.io.*;

public class UniversityStudent implements Observer {
    Subject subject;
    File myFile;
    UniversityStudent(Subject subject, String fileName) {
        this.subject = subject;
        subject.addObserver(this);
        myFile = new File(fileName);
    }
    public void hearTelephone(String message) {
        try {
            RandomAccessFile out = new RandomAccessFile(myFile, "rw");
            out.seek(out.length());
            byte [] messageBytes = message.getBytes();
            out.write(messageBytes);
            out.write("\n".getBytes());
            System.out.println("I am a university student, I have received the message: " + message);
            System.out.println("The message has been written to my file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
