package designpatterns.observer;

import java.util.ArrayList;

public class SeekJobCenter implements Subject{
    private String message;
    private boolean changed;
    ArrayList<Observer> personList;

    SeekJobCenter() {
        personList = new ArrayList<Observer>();
        message = "";
        changed = false;
    }
    public void addObserver(Observer observer) {
        if (!personList.contains(observer)) {
            personList.add(observer);
        }
    }
    public void deleteObserver(Observer observer) {
        if (personList.contains(observer)) {
            personList.remove(observer);
        }
    }
    public void notifyObservers() {
        if (changed) {
            for (int i = 0; i < personList.size(); i++) {
                Observer observer = personList.get(i);
                observer.hearTelephone(message);
            }
            changed = false;
        }
    }
    public void giveNewMessage(String newMessage) {
        if (newMessage.equals(message)) {
            changed = false;
        } else {
            message = newMessage;
            changed = true;
        }
    }
}
