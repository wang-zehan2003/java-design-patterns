package designpatterns.observer;

import java.io.File;
import java.io.RandomAccessFile;

public class HaiGui implements Observer{
    Subject subject;
    File myFile;
    HaiGui(Subject subject, String fileName) {
        this.subject = subject;
        subject.addObserver(this);
        myFile = new File(fileName);
    }
    public void hearTelephone(String message) {
        try {
            boolean boo = message.contains("java程序员") || message.contains("软件");
            if (boo) {
                RandomAccessFile out = new RandomAccessFile(myFile, "rw");
                out.seek(out.length());
                byte [] messageBytes = message.getBytes();
                out.write(messageBytes);
                out.write("\n".getBytes());
                System.out.println("我是海归，我已经收到了招聘信息");
                System.out.println("我将招聘信息保存到了文件" + myFile.getName() + "中");
                System.out.println("内容为：" + message);
            } else {
                System.out.println("我是海归，这次的信息中没有我需要的信息");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
