package designpatterns.factorypattern;

public class BluePenCore extends PenCore{
    public BluePenCore() {
        color = "蓝色";
    }

    @Override
    public void writeWord(String s) {
        System.out.println("写出" + color + "的字：" + s);
    }
}
