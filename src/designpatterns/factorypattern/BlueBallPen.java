package designpatterns.factorypattern;

public class BlueBallPen extends BallPen{
    @Override
    public PenCore getPenCore() {
        return new BluePenCore();
    }
}
