package designpatterns.factorypattern;

public class RedBallPen extends BallPen{
    @Override
    public PenCore getPenCore() {
        return new RedPenCore();
    }
}
