package designpatterns.statepattern;
public  interface  TemperatureState{
      public void showTemperature();   
}  
