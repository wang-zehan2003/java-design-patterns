package designpatterns.stragetypattern;

public class EncodeContext {
    EncryptStrategy encryptStrategy;
    public void setEncryptStrategy(EncryptStrategy encryptStrategy) {
        this.encryptStrategy = encryptStrategy;
    }
    public void encryptFile(java.io.File file) {
        encryptStrategy.encryptFile(file);
    }
    public String decryptFile(java.io.File file) {
        return encryptStrategy.decryptFile(file);
    }
}
