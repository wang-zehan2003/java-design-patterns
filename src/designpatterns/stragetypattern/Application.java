package designpatterns.stragetypattern;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Application {
    public static void main(String[] args) {
        File fileOne = new File("A.txt");
        File fileTwo = new File("B.txt");
        String s = "";
        EncodeContext encodeContext = new EncodeContext();
        encodeContext.setEncryptStrategy(new StrategyOne("你好 Hello"));
        encodeContext.encryptFile(fileOne);
        System.out.println(fileOne.getName() + " 加密后的内容：");
        try {
            FileReader fileReader = new FileReader(fileOne);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((s = bufferedReader.readLine()) != null) {
                System.out.println(s);
            }
            bufferedReader.close();
            fileReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String str = encodeContext.decryptFile(fileOne);
        System.out.println(fileOne.getName() + " 解密后的内容：");
        System.out.println(str);
        encodeContext.setEncryptStrategy(new StrategyTwo("世界 World"));
        encodeContext.encryptFile(fileTwo);
        System.out.println(fileTwo.getName() + " 加密后的内容：");
        try {
            FileReader fileReader = new FileReader(fileTwo);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((s = bufferedReader.readLine()) != null) {
                System.out.println(s);
            }
            bufferedReader.close();
            fileReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
