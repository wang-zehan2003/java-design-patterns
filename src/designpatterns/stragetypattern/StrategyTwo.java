package designpatterns.stragetypattern;

import java.io.File;

public class StrategyTwo implements EncryptStrategy{
    String password;
    public StrategyTwo() {
        this.password = "I love this game.";
    }
    StrategyTwo(String password) {
        if (password.length() == 0) {
            this.password = "I love this game.";
        }
        this.password = password;
    }
    @Override
    public void encryptFile(File file) {
        try {
            byte[] a = password.getBytes();
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
            byte[] b = new byte[(int) file.length()];
            int m = fileInputStream.read(b);
            for (int i = 0; i < m; i++) {
                int n = b[i] ^ a[i % a.length];
                b[i] = (byte) n;
            }
            fileInputStream.close();
            java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(file);
            fileOutputStream.write(b, 0, m);
            fileOutputStream.close();
        } catch (java.io.FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (java.io.IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String decryptFile(File file) {
        try {
            byte[] a = password.getBytes();
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
            byte[] b = new byte[(int) file.length()];
            int m = fileInputStream.read(b);
            for (int i = 0; i < m; i++) {
                int n = b[i] ^ a[i % a.length];
                b[i] = (byte) n;
            }
            fileInputStream.close();
            java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(file);
            fileOutputStream.write(b, 0, m);
            fileOutputStream.close();
            return new String(b, 0, m);
        } catch (java.io.FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (java.io.IOException e) {
            throw new RuntimeException(e);
        }
    }
}
