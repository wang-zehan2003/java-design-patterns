package experiment.exp1;

class Simple{
    void message(String s){
        System.out.println(s);}
}

class Test1{
    public static void main(String args[]){
        try{
            Class c=Class.forName("Simple");
            Simple s=(Simple)c.newInstance();
            s.message("Hello Java");

        }catch(Exception e){System.out.println(e);}

    }
}
