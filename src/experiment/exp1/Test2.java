package experiment.exp1;

// 定义一个类Test2，动态获取Simple类中定义的函数信息，包括函数名、返回值类型、参数类型等，并输出。
public class Test2 {
    public static void main(String args[]) {
        try {
            // 获取Simple类的Class对象
            Class c = Class.forName("experiment.exp1.Simple");
            // 获取Simple类中定义的所有函数信息
            java.lang.reflect.Method[] methods = c.getDeclaredMethods();
            // 遍历所有函数信息
            for (java.lang.reflect.Method method : methods) {
                // 输出函数名
                System.out.println("Method Name: " + method.getName());
                // 输出返回值类型
                System.out.println("Return Type: " + method.getReturnType());
                // 输出参数类型
                Class[] parameterTypes = method.getParameterTypes();
                System.out.print("Parameter Types: ");
                for (Class parameterType : parameterTypes) {
                    System.out.print(parameterType + " ");
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
